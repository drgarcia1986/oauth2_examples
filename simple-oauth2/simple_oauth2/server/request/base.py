# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'


class RequestAdapterBase():
    def __init__(self):
        self.server_grants_list = None
        self.authorization_type = None
        self.authorization = None
        self.arguments = None
        self.grant = None
        self.scopes = None
        self.token = None
        self.request = None

    def read_authorization(self):
        raise NotImplemented

    def read_arguments(self):
        raise NotImplemented

    def read_grant(self):
        raise NotImplemented

    def read_scopes(self):
        raise NotImplemented

    def read_token(self):
        raise NotImplemented

    def clear(self):
        self.authorization_type = None
        self.authorization = None
        self.arguments = None
        self.grant = None
        self.scopes = None
        self.token = None
        self.request = None

    def update_request(self, request):
        self.clear()
        self.request = request