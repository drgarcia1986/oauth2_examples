# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

from simple_oauth2.server.client.base import ClientControllerBase, ClientNotFound


class ClientControllerMongoDB(ClientControllerBase):
    def __init__(self, mongo_collection, client_id_length=20, client_secret_length=40):
        super().__init__(client_id_length, client_secret_length)
        self.mongo_collection = mongo_collection

    def save(self, client):
        self.mongo_collection.update({'application': client['application']}, client, upsert=True)
        return True

    def __get_by(self, field, param):
        doc = self.mongo_collection.find_one({field: param})
        if not doc:
            raise ClientNotFound
        return doc

    def get_by_id(self, client_id):
        return self.__get_by('client_id', client_id)

    def get_by_name(self, name):
        return self.__get_by('application', name)
