# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

import pickle
from simple_oauth2.server.token.base import TokenControllerBase, TokenNotFound
from simple_oauth2.utils.types import GrantTypes


class KeysTypes():
    token = 'token'
    client = 'client'


class TokenControllerRedis(TokenControllerBase):
    def __init__(self, lifetime=86400, redis=None):
        super().__init__(lifetime)
        self.redis = redis

    # TODO: Essa provavelmente será a solução para geração de chaves quando entrar outros Grants
    def __generate_key(self, type_of_key, **kwargs):
        key = 'oauth2_{}'.format('tokens' if type_of_key == KeysTypes.token else 'tokens_clients')
        if kwargs['grant_type'] == GrantTypes.client_credentials:
            key += '_{}'.format(kwargs['token'] if type_of_key == KeysTypes.token else kwargs['client_id'])
        return key

    def __generate_key_token(self, token):
        return 'oauth2_tokens_{}'.format(token)

    def __generate_key_client(self, client_id):
        return 'oauth2_tokens_client_{}'.format(client_id)

    def save(self, token_info, expire_at):
        self.revoke_by_client(token_info['client_id'])

        pipe = self.redis.pipeline()
        key = self.__generate_key_token(token_info['token'])
        pipe.set(key, pickle.dumps(token_info))
        pipe.expireat(key, expire_at)

        key_client = self.__generate_key_client(token_info['client_id'])
        pipe.set(key_client, token_info['token'])
        pipe.expireat(key_client, expire_at)

        pipe.execute()
        return True

    def __delete_client_and_token_register(self, client_id, token):
        pipe = self.redis.pipeline()
        pipe.delete(self.__generate_key_client(client_id))
        pipe.delete(self.__generate_key_token(token))
        pipe.execute()
        return True

    def revoke_by_token(self, token):
        try:
            token_info = self.get(token)
            if token_info:
                self.__delete_client_and_token_register(token_info['client_id'], token)
            return True
        except Exception:
            return True

    def revoke_by_client(self, client_id):
        token = self.redis.get(self.__generate_key_client(client_id))
        if token:
            self.__delete_client_and_token_register(client_id, token.decode())
        return True

    def get(self, token):
        key = self.__generate_key_token(token)
        token_info = self.redis.get(key)
        if not token_info:
            raise TokenNotFound
        return pickle.loads(token_info)
