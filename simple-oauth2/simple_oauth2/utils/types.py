#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'


class GrantTypes():
    client_credentials = 'client_credentials'

    @staticmethod
    def is_valid(grant):
        return grant in [GrantTypes.client_credentials]