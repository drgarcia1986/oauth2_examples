# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

import datetime
from simple_oauth2.utils.generators import token_generator


class TokenNotFound(Exception):
    pass


class TokenControllerBase():
    def __init__(self, lifetime=86400):
        self.lifetime = lifetime

    def create(self, client_id, grant, scopes, user_id=None):
        expires_at = datetime.datetime.now() + datetime.timedelta(seconds=self.lifetime)
        # TODO: Questão complexa, mas client credentials não deve ter refresh token
        refresh_token = None
        token = dict()
        token['client_id'] = client_id
        token['token'] = token_generator()
        token['token_type'] = 'Bearer'
        token['expires_at'] = expires_at.strftime('%d/%m/%Y %H:%M:%S')
        token['expires_in'] = self.lifetime
        token['refresh_expires_at'] = ''
        token['grant_type'] = grant
        token['user_id'] = user_id  # Resource Owner
        token['refresh_token'] = refresh_token
        token['scopes'] = scopes if scopes else []

        self.save(token, expires_at)
        return token

    def save(self, token_info, expire_at):
        raise NotImplemented

    def get(self, token):
        raise NotImplemented

    def revoke_by_token(self, token):
        raise NotImplemented

    def revoke_by_client(self, client_id):
        raise NotImplemented