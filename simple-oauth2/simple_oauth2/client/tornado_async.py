# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

from simple_oauth2.client.base import OAuth2HTTPClientBase, OAuth2HTTPResponseError
import tornado.httpclient
import tornado.gen
import urllib.parse
import datetime
import json


class OAuth2HTTPClientAsync(OAuth2HTTPClientBase):
    def __init__(self, server, uri_token, uri_revoke_token, grant_type, client_id, client_secret=None,
                 scope=None, security=True, authorization_header=True,
                 token=None, token_type=None, token_expiration=None):
        super().__init__(server, uri_token, uri_revoke_token, grant_type, client_id, client_secret,
                         scope, security, authorization_header, token, token_type, token_expiration)
        self.http_client = tornado.httpclient.AsyncHTTPClient()

    @tornado.gen.coroutine
    def make_request(self, uri, headers, method, body):
        request = tornado.httpclient.HTTPRequest(url='{}://{}{}'.format(self.protocol, self.server, uri),
                                                 headers=headers,
                                                 method=method,
                                                 body=body)
        try:
            response = yield self.http_client.fetch(request)
            return self.process_body_response(response.code, response.reason, response.headers, response.body.decode())
        except tornado.httpclient.HTTPError as err:
            return self.process_body_response(err.code, err.response.reason, err.response.headers, err.response.body.decode())

    def make_request_encode_multipart_formdata(self, uri, headers, method, body):
        raise NotImplemented

    @tornado.gen.coroutine
    def get_token_client_credentials(self):
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        body = {"grant_type": "client_credentials"}
        if self.scope:
            body.update({'scope': ' '.join(self.scope)})
        self.make_authorization(headers, body)
        body = urllib.parse.urlencode(body)

        response = yield self.make_request(uri=self.uri_token,
                                           headers=headers,
                                           method='POST',
                                           body=body)

        if response.status != 200:
            raise OAuth2HTTPResponseError(response.status, response.reason, response.content)

        self.token = response.content['access_token']
        self.token_type = response.content['token_type']
        seconds = response.content.get('expires_in', None)
        if seconds:
            self.token_expiration = datetime.datetime.now() + datetime.timedelta(seconds=seconds)

        return True

    @tornado.gen.coroutine
    def consume_resource(self, uri, method='GET', body=None):
        if not self.token or self.token_expired():
            self.get_token()

        headers = {'Authorization': '{} {}'.format(self.token_type, self.token)}
        if body:
            headers.update({'Content-Type': 'application/json'})

        body = json.dumps(body) if body else None

        response = yield self.make_request(uri=uri,
                                           method=method,
                                           headers=headers,
                                           body=body)

        return response

    @tornado.gen.coroutine
    def revoke_token(self):
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        body = {"access_token": self.token}
        self.make_authorization(headers, body)
        body = urllib.parse.urlencode(body)

        response = yield self.make_request(uri=self.uri_revoke_token,
                                           headers=headers,
                                           method='POST',
                                           body=body)

        if response.status == 200:
            self.token, self.token_expiration = None, None
            return True
        else:
            raise OAuth2HTTPResponseError(response.status, response.reason, response.content)