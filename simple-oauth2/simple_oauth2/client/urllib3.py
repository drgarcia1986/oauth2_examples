# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

from simple_oauth2.client.base import OAuth2HTTPClientBase
import urllib3


class OAuth2HTTPClientUrllib3(OAuth2HTTPClientBase):
    def __init__(self, server, uri_token, uri_revoke_token, grant_type, client_id, client_secret=None,
                 scope=None, security=True, authorization_header=True,
                 token=None, token_type=None, token_expiration=None):
        super().__init__(server, uri_token, uri_revoke_token, grant_type, client_id, client_secret,
                         scope, security, authorization_header, token, token_type, token_expiration)
        self.http_client = urllib3.PoolManager()

    def make_request(self, uri, headers, method, body):
        response = self.http_client.urlopen(url='{}://{}{}'.format(self.protocol, self.server, uri),
                                            headers=headers,
                                            method=method,
                                            body=body)
        return self.process_body_response(response.status, response.reason, response.headers, response.data.decode())

    def make_request_encode_multipart_formdata(self, uri, headers, method, body):
        del headers['Content-Type']
        response = self.http_client.request_encode_body(method=method,
                                                        url='{}://{}{}'.format(self.protocol, self.server, uri),
                                                        fields=body,
                                                        headers=headers)
        return self.process_body_response(response.status, response.reason, response.headers, response.data.decode())
