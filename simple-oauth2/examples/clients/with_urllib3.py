# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

from simple_oauth2.client.urllib3 import OAuth2HTTPClientUrllib3
from simple_oauth2.utils.types import GrantTypes
import json
import urllib3
import datetime


def register_application(protocol, server, uri, name, grants=None, scopes=None):
    http = urllib3.PoolManager()
    headers = {'Content-Type': "application/json"}
    response = http.urlopen(method='POST',
                            url='{}://{}{}'.format(protocol, server, uri),
                            headers=headers,
                            body=json.dumps({'application': name,
                                             'authorized_grants': grants,
                                             'scopes': scopes}))

    if response.status != 201:
        raise Exception

    info_return = dict()
    if response.data:
        info_return = json.loads(response.data.decode())

    return info_return['client_id'], info_return['client_secret']


if __name__ == "__main__":
    print('start at: {}'.format(datetime.datetime.now()))
    try:
        print('Registering Application')
        CLIENT_ID, CLIENT_SECRET = register_application('http', 'localhost:8888', '/clients', 'Acme', ['client_credentials'], ['foo'])
        print('client_id: {}\nclient_secret: {}'.format(CLIENT_ID, CLIENT_SECRET))

        print('Get Token')
        oauth2client = OAuth2HTTPClientUrllib3('localhost:8888', '/oauth/token', '/oauth/token/revoke',
                                               GrantTypes.client_credentials, CLIENT_ID, CLIENT_SECRET,
                                               ['foo'], False, True)
        oauth2client.get_token()
        print('token: {}'.format(oauth2client.token))

        print('Consume FOO')
        foo = oauth2client.consume_resource('/foo')
        print("{status} {reason}\n{content}".format(**foo.__dict__))

        print('Consume BAR')
        bar = oauth2client.consume_resource('/bar')
        print("{status} {reason}\n{content}".format(**bar.__dict__))

        print('Revoke Token')
        oauth2client.revoke_token()
        print('Token revoked')

        print('Consume FOO')
        foo = oauth2client.consume_resource('/foo')
        print("{status} {reason}\n{content}".format(**foo.__dict__))
        print("New token {}".format(oauth2client.token))

        print('Testing token controller')
        old_token, old_token_type = oauth2client.token, oauth2client.token_type
        oauth2client.token, oauth2client.token_type = None, None
        oauth2client.get_token()
        oauth2client.token, oauth2client.token_type = old_token, old_token_type
        foo = oauth2client.consume_resource('/foo')
        print("{status} {reason}\n{content}".format(**foo.__dict__))

    except Exception as err:
        print(err)
    print('stop at: {}'.format(datetime.datetime.now()))