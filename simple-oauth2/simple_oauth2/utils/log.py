# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

import logging


server_client_log = logging.getLogger("oauth2.server.client")
server_request_log = logging.getLogger("oauth2.server.request")
server_token_log = logging.getLogger("oauth2.server.token")
server_controller_log = logging.getLogger("oauth2.server.controller")
client_http_log = logging.getLogger("oauth2.client.http")