# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

import json
import tornado.web
import tornado.ioloop
import fakeredis
import mongomock
from simple_oauth2.server.client.mongodb import ClientControllerMongoDB
from simple_oauth2.server.token.redis import TokenControllerRedis
from simple_oauth2.server.request.tornado import RequestAdapterTornado
from simple_oauth2.server.controller import OAuth2Controller, InvalidGrantType
from simple_oauth2.utils.types import GrantTypes


class OAuth2TokenHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.oauth2_controller = self.settings['oauth2_controller']

    def post(self):
        try:
            token = self.oauth2_controller.validate_and_generate_token(self.request)
            self.set_status(200)
            self.set_header('Content-Type', 'application/json')
            self.finish(json.dumps({'access_token': token['token'],
                                    'token_type': token['token_type'],
                                    'expires_in': token['expires_in'],
                                    'expires_at': token['expires_at']}))
        except Exception as err:
            self.set_status(401)
            self.finish(str(err))


class OAuth2TokenRevokeHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.oauth2_controller = self.settings['oauth2_controller']

    def post(self):
        try:
            token = self.oauth2_controller.revoke_token(self.request)
            self.set_status(200)
            self.set_header('Content-Type', 'application/json')
            self.finish(json.dumps({"access_token": token}))
        except Exception as err:
            self.set_status(401)
            self.finish(str(err))


class ClientsHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.oauth2_controller = self.settings['oauth2_controller']

    def post(self):
        try:
            body = json.loads(self.request.body.decode())
            for grant in body['authorized_grants']:
                if not GrantTypes.is_valid(grant):
                    raise InvalidGrantType
            client = self.oauth2_controller.client_controller.create(body['application'],
                                                                     body.get('redirect_uris', []),
                                                                     body['authorized_grants'],
                                                                     body.get('scopes', []))
            self.set_status(201)
            self.set_header('Content-Type', 'application/json')
            self.finish(json.dumps(client))
        except Exception as err:
            self.set_status(500)
            self.finish(json.dumps({'msg': err.__class__.__name__}))


class BaseHandler(tornado.web.RequestHandler):
    def initialize(self, scope):
        self.scope = scope
        self.oauth2_controller = self.settings['oauth2_controller']

    def prepare(self):
        try:
            self.oauth2_controller.validate_token(self.request, self.scope)
        except Exception as err:
            self.set_status(401)
            self.finish(err.__class__.__name__)


class FooHandler(BaseHandler):
    def get(self):
        self.finish(json.dumps({'msg': 'This is Foo'}))


class BarHandler(BaseHandler):
    def get(self):
        self.finish(json.dumps({'msg': 'This is Bar'}))


def main():
    # Connect to storages
    redis_server = fakeredis.FakeRedis()
    mongo_server = mongomock.MongoClient()

    # Create client controller
    client_controller = ClientControllerMongoDB(mongo_collection=mongo_server['db']['oauth_clients'])

    # Create Token controller
    token_controller = TokenControllerRedis(redis=redis_server)

    # Create Request Adapter
    request_adapter = RequestAdapterTornado

    # Create OAuth2 controller
    oauth2_controller = OAuth2Controller(client_controller=client_controller,
                                         token_controller=token_controller,
                                         request_adapter=request_adapter,
                                         grant_list=[GrantTypes.client_credentials],
                                         validate_scope_in_client_credentials=True)

    app = tornado.web.Application([
        (r'/oauth/token', OAuth2TokenHandler),
        (r'/oauth/token/revoke', OAuth2TokenRevokeHandler),
        (r'/clients', ClientsHandler),
        (r'/foo', FooHandler, dict(scope='foo')),
        (r'/bar', BarHandler, dict(scope='bar'))],
        oauth2_controller=oauth2_controller)

    app.listen(8888)
    print('start tornado server')
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()