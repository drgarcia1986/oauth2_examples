# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

import json
from simple_oauth2.server.request.base import RequestAdapterBase
from simple_oauth2.server.controller import InvalidGrantType
from simple_oauth2.server.token.base import TokenNotFound


class RequestAdapterTornado(RequestAdapterBase):
    def read_authorization(self):
        credentials = self.request.headers.get('Authorization', None)
        if credentials:
            self.authorization_type, self.authorization = credentials.split(' ')
            return True

    def read_arguments(self):
        self.arguments = {}
        content_type = self.request.headers.get('Content-Type', None)
        if content_type and content_type.startswith('application/json'):
            self.arguments = json.loads(self.request.body.decode())
        else:
            self.arguments = {key: value[0].decode() for key, value in self.request.arguments.items()}
        return True

    def read_grant(self):
        self.grant = self.arguments.get('grant_type', None)
        if not self.grant or self.grant not in self.server_grants_list:
            raise InvalidGrantType
        return True

    def read_scopes(self):
        scope = self.arguments.get('scope', None)
        if scope:
            self.scopes = list(scope.split(' '))

    def read_token(self):
        self.token = self.authorization if self.authorization_type == 'Bearer' else self.arguments.get('access_token', None)
        if not self.token:
            raise TokenNotFound
        return True