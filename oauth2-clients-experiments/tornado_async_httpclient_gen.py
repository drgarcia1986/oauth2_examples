# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

import tornado.httpclient
import tornado.gen
import tornado.ioloop
import tornado.web
import tornado
import urllib3
import json
import base64
import urllib.parse
import datetime
from simple_oauth2.utils.types import GrantTypes


class OAuth2HTTPResponse():
    def __init__(self, status, reason, content=None):
        self.status = status
        self.reason = reason
        self.content = content


class OAuth2HTTPResponseError(OAuth2HTTPResponse, Exception):
    pass


class OAuth2AsyncHTTPClient():

    def __init__(self, server, uri_token, grant_type, client_id, client_secret=None,
                 scope=None, security=True, authorization_header=True):
        self.grant_type = grant_type
        self.client_id = client_id
        self.client_secret = client_secret
        self.server = server
        self.uri_token = uri_token
        self.scope = scope
        self.authorization_header = authorization_header
        self.protocol = 'https' if security else 'http'
        self.token = None
        self.token_type = None
        self.token_expiration = None
        self.__token_strategy = {GrantTypes.client_credentials: self.__get_token_client_credentials}
        self.http_client = tornado.httpclient.AsyncHTTPClient()

    def make_authorization(self, headers, body):
        if self.authorization_header:
            authorization = base64.b64encode(bytes('{}:{}'.format(self.client_id, self.client_secret), 'utf-8'))
            headers.update({'Authorization': 'Basic {}'.format(authorization.decode())})
        else:
            body.update({"client_id": self.client_id, "client_secret": self.client_secret})

    def token_expired(self):
        if self.token_expiration:
            return self.token_expiration <= datetime.datetime.now()
        else:
            return True

    def __process_body_response(self, response):
        if 'application/json' in response.headers['Content-Type'].lower():
            body_response = json.loads(response.body.decode())
        else:
            try:
                body_response = {values.split('=')[0]: values.split('=')[1]
                                 for values in response.body.decode().split('&')}
            except:
                body_response = response.body.decode()
        return body_response

    @tornado.gen.coroutine
    def __get_token_client_credentials(self):
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        body = {"grant_type": "client_credentials"}
        if self.scope:
            body.update({'scope': ' '.join(self.scope)})
        self.make_authorization(headers, body)
        body = urllib.parse.urlencode(body)

        request = tornado.httpclient.HTTPRequest(url='{}://{}{}'.format(self.protocol, self.server, self.uri_token),
                                                 headers=headers,
                                                 method='POST',
                                                 body=body)
        response = yield self.http_client.fetch(request)

        body_response = self.__process_body_response(response)

        if response.code != 200:
            raise OAuth2HTTPResponseError(response.code, response.reason, body_response)

        self.token = body_response['access_token']
        self.token_type = body_response['token_type']
        seconds = body_response.get('expires_in', None)
        if seconds:
            self.token_expiration = datetime.datetime.now() + datetime.timedelta(seconds=seconds)

        return True

    @tornado.gen.coroutine
    def revoke_token(self):
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        body = {"access_token": self.token}
        self.make_authorization(headers, body)
        body = urllib.parse.urlencode(body)

        request = tornado.httpclient.HTTPRequest(url='{}://{}{}'.format(self.protocol, self.server, self.uri_token),
                                                 headers=headers,
                                                 method='DELETE',
                                                 body=body)

        response = yield self.http_client.fetch(request)

        if response.code == 200:
            self.token, self.token_expiration = None, None
            return True
        else:
            raise OAuth2HTTPResponseError(response.code, response.reason, response.body)

    def get_token(self):
        func = self.__token_strategy[self.grant_type]
        return func()

    @tornado.gen.coroutine
    def consume_resource(self, uri, method='GET', body=None):
        if not self.token or self.token_expired():
            self.get_token()

        headers = {'Authorization': '{} {}'.format(self.token_type, self.token)}
        if body:
            headers.update({'Content-Type': 'application/json'})

        body = json.dumps(body) if body else None

        request = tornado.httpclient.HTTPRequest(url='{}://{}{}'.format(self.protocol, self.server, uri),
                                                 method=method,
                                                 headers=headers,
                                                 body=body)

        response = yield self.http_client.fetch(request)

        body_response = self.__process_body_response(response)
        return OAuth2HTTPResponse(response.code, response.reason, body_response)


def register_application(protocol, server, uri, name, grants=None, scopes=None):
    http = urllib3.PoolManager()
    headers = {'Content-Type': "application/json"}
    response = http.urlopen(method='POST',
                            url='{}://{}{}'.format(protocol, server, uri),
                            headers=headers,
                            body=json.dumps({'application': name,
                                             'authorized_grants': grants,
                                             'scopes': scopes}))

    if response.status != 201:
        raise Exception

    info_return = dict()
    if response.data:
        info_return = json.loads(response.data.decode())

    return info_return['client_id'], info_return['client_secret']


class MainOAuthHandler(tornado.web.RequestHandler):
    def initialize(self, oauth_client):
        self.oauth_client = oauth_client

    @tornado.gen.coroutine
    def get(self, resource):
        retorno = yield self.oauth_client.consume_resource('/{}'.format(resource))
        self.write(json.dumps(retorno.content))
        self.finish()

    @tornado.gen.coroutine
    def post(self, resource):
        yield self.oauth_client.get_token()
        self.write(self.oauth_client.token)
        self.finish()


class AnotherHandler(tornado.web.RequestHandler):
    def get(self):
        self.finish("bla")


if __name__ == "__main__":
    try:
        print('Registering Application')
        CLIENT_ID, CLIENT_SECRET = register_application('http', 'localhost:8888', '/clients', 'Acme', ['client_credentials'], ['foo', 'bar'])
        print('client_id: {}\nclient_secret: {}'.format(CLIENT_ID, CLIENT_SECRET))

        oauth2client = OAuth2AsyncHTTPClient('localhost:8888', '/oauth/token', GrantTypes.client_credentials,
                                             CLIENT_ID, CLIENT_SECRET, ['foo', 'bar'], False, True)

        application = tornado.web.Application([(r"/c/([^/]+)", MainOAuthHandler, dict(oauth_client=oauth2client)),
                                               (r"/bla", AnotherHandler)])
        application.listen(9999)
        tornado.ioloop.IOLoop.instance().start()

    except Exception as err:
        print(err)