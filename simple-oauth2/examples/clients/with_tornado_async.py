# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

import tornado.gen
import tornado.ioloop
import tornado.web
import tornado.httpclient
import json
from simple_oauth2.utils.types import GrantTypes
from simple_oauth2.client.tornado_async import OAuth2HTTPClientAsync


class MainOAuthHandler(tornado.web.RequestHandler):
    def initialize(self, oauth2client):
        self.oauth2client = oauth2client

    @tornado.gen.coroutine
    def get(self, resource):
        retorno = yield self.oauth2client.consume_resource('/{}'.format(resource))
        self.set_status(retorno.status)
        self.write(json.dumps(retorno.content))
        self.finish()

    @tornado.gen.coroutine
    def post(self):
        yield self.oauth2client.get_token()
        self.write(self.oauth2client.token)
        self.finish()


class FooHandler(tornado.web.RequestHandler):
    def get(self):
        self.finish("Foo")


def register_application(protocol, server, uri, name, grants=None, scopes=None):
    http = tornado.httpclient.HTTPClient()
    headers = {'Content-Type': "application/json"}
    request = tornado.httpclient.HTTPRequest(method='POST',
                                             url='{}://{}{}'.format(protocol, server, uri),
                                             headers=headers,
                                             body=json.dumps({'application': name,
                                                              'authorized_grants': grants,
                                                              'scopes': scopes}))
    response = http.fetch(request)

    if response.code != 201:
        raise Exception

    info_return = dict()
    if response.body:
        info_return = json.loads(response.body.decode())

    return info_return['client_id'], info_return['client_secret']


if __name__ == "__main__":
    try:
        print('Registering Application')
        CLIENT_ID, CLIENT_SECRET = register_application('http', 'localhost:8888', '/clients', 'Acme', ['client_credentials'], ['foo'])
        print('client_id: {}\nclient_secret: {}'.format(CLIENT_ID, CLIENT_SECRET))

        oauth2client = OAuth2HTTPClientAsync('localhost:8888', '/oauth/token', '/oauth/token/revoke',
                                             GrantTypes.client_credentials, CLIENT_ID, CLIENT_SECRET,
                                             ['foo'], security=False, True)

        application = tornado.web.Application([(r"/oauth/([^/]+)", MainOAuthHandler, dict(oauth2client=oauth2client)),
                                               (r"/oauth", MainOAuthHandler, dict(oauth2client=oauth2client)),
                                               (r"/foo", FooHandler)])
        application.listen(9999)
        tornado.ioloop.IOLoop.instance().start()

    except Exception as err:
        print(err)


'''
# GetToken:
curl -i -X POST http://localhost:9999/oauth

HTTP/1.1 200 OK
Date: Tue, 02 Dec 2014 22:19:48 GMT
Content-Length: 36
Content-Type: text/html; charset=UTF-8
Server: TornadoServer/4.0.2
Proxy-Connection: keep-alive
Connection: keep-alive

a4750c9f-dd3c-44c0-bfb0-9093821b4284

#Consume Resource
curl -i http://localhost:9999/oauth/foo

HTTP/1.1 200 OK
Date: Tue, 02 Dec 2014 22:20:17 GMT
Content-Length: 28
Etag: "294ce739d4f5c3208042da24eb3bd1955c8bccaa"
Content-Type: text/html; charset=UTF-8
Server: TornadoServer/4.0.2
Proxy-Connection: keep-alive
Connection: keep-alive

'{"msg": "This is Foo"}'

#Consume Resource out of scope
curl -i http://localhost:9999/oauth/bar

HTTP/1.1 401 Unauthorized
Content-Length: 26
Date: Tue, 02 Dec 2014 22:22:00 GMT
Content-Type: text/html; charset=UTF-8
Server: TornadoServer/4.0.2
Proxy-Connection: keep-alive
Connection: keep-alive

"ResourceNotInClientScope"
'''
