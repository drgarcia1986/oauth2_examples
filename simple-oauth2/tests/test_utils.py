# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

import unittest
from simple_oauth2.utils.types import GrantTypes
from simple_oauth2.utils.generators import *


class TestUtilsTypes(unittest.TestCase):
    def test_grants_types(self):
        self.assertEqual('client_credentials', GrantTypes.client_credentials)

    def test_grant_is_valid(self):
        for grant in ['client_credentials']:
            self.assertTrue(GrantTypes.is_valid(grant))


class TestUtilsGenerators(unittest.TestCase):
    def test_id_generator(self):
        first_id = id_generator(10)
        second_id = id_generator(10)
        self.assertNotEqual(first_id, second_id)
        self.assertTrue(len(first_id), 10)
        self.assertTrue(len(second_id), 10)

    def test_id_generator_unique(self):
        l = []
        for _ in range(50):
            l.append(id_generator(20))
        self.assertTrue(len(set(l)) == len(l))


if __name__ == '__main__':
    unittest.main()
