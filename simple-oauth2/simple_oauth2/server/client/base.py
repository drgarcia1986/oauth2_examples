# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

from simple_oauth2.utils.generators import id_generator


class ClientAlreadyExists(Exception):
    pass


class InvalidClientCredentials(Exception):
    pass


class ClientNotFound(Exception):
    pass


class ClientUnauthorized(Exception):
    pass


class ResourceNotInClientScope(Exception):
    pass


class ClientControllerBase():
    def __init__(self, client_id_length=20, client_secret_length=40):
        self.client_id_length = client_id_length
        self.client_secret_length = client_secret_length

    def save(self, client):
        raise NotImplemented

    def check_client_exists(self, name):
        try:
            return True if self.get_by_name(name) else False
        except ClientNotFound:
            return False

    def create(self, application, redirect_uris=None, authorized_grants=None, scopes=None):
        if self.check_client_exists(application):
            raise ClientAlreadyExists
        client = dict()
        client['application'] = application
        client['client_id'] = id_generator(self.client_id_length)
        client['client_secret'] = id_generator(self.client_secret_length)
        client['redirect_uris'] = redirect_uris if redirect_uris else []
        client['authorized_grants'] = authorized_grants if authorized_grants else []
        client['scopes'] = scopes if scopes else []
        self.save(client)
        return client

    def update(self, name, redirect_uris=None, authorized_grants=None, scopes=None, regenerate_keys=False):
        client = self.get_by_name(name)
        if regenerate_keys:
            client['client_id'] = id_generator(self.client_id_length)
            client['client_secret'] = id_generator(self.client_secret_length)
        if redirect_uris:
            client['redirect_uris'] = redirect_uris
        if authorized_grants:
            client['authorized_grants'] = authorized_grants
        if scopes:
            client['scopes'] = scopes
        self.save(client)
        return client

    def get_by_id(self, client_id):
        raise NotImplemented

    def get_by_name(self, name):
        raise NotImplemented