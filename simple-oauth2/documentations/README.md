Client Credentials Flow
-------
Functional in the tornado server example


![client credentials diagram](/simple-oauth2/documentations/img/client_credentials_flow.png?raw=true)

Image source: [understanding-oauth2](http://www.bubblecode.net/en/2013/03/10/understanding-oauth2/)

### Create Client
#### Request
```
curl -i http://localhost:8888/clients -X POST -H 'Content-Type: application/json' -d '{"application": "Acme", "authorized_grants": ["client_credentials"], "scopes": ["foo"]}'
```
#### Response
```http
HTTP/1.1 201 Created
Server: TornadoServer/4.0.2
Date: Wed, 26 Nov 2014 19:23:55 GMT
Content-Type: application/json
Content-Length: 199
Proxy-Connection: keep-alive
Connection: keep-alive

{'redirect_uris': [], 'scopes': ['foo'], 'client_secret': 'YWcyjP23pk3FrsleLQ1UhNJdLCVMas81vvxxtUU5', 'application': 'Acme', 'client_id': 'aRuRRAtNlXALpyXxlbZY', 'authorized_grants': ['client_credentials']}
```

### Create Token
#### Request
```
curl -i http://localhost:8888/oauth/token -X POST -H 'Content-Type: application/x-www-form-urlencoded' -H 'Authorization: Basic YVJ1UlJBdE5sWEFMcHlYeGxiWlk6WVdjeWpQMjNwazNGcnNsZUxRMVVoTkpkTENWTWFzODF2dnh4dFVVNQ==' -d 'grant_type=client_credentials&scope=foo'
```

#### Response
```http
HTTP/1.1 200 Created
Server: TornadoServer/4.0.2
Date: Wed, 26 Nov 2014 19:24:38 GMT
Content-Type: application/json
Content-Length: 117
Proxy-Connection: keep-alive
Connection: keep-alive

{"token_type": "Bearer", "access_token": "a2f5b1cd-437a-4aa8-b52f-f2047f16f062", "expires_at": "28/11/2014 11:44:48"}
```

### Consume resource
#### Request
```
curl -i http://localhost:8888/foo -H 'Authorization: Bearer 1523f55f-381c-4395-b004-93f4991ca208'
```
#### Response
```http
HTTP/1.1 200 OK
Server: TornadoServer/4.0.2
Date: Wed, 26 Nov 2014 19:25:05 GMT
Content-Type: text/html; charset=UTF-8
Etag: "1fbcdda31be4738594fb51cf8c19506c5522be86"
Content-Length: 22
Proxy-Connection: keep-alive
Connection: keep-alive

{"msg": "This is Foo"}
```

### Consume resource (out of scope)
#### Request
```
curl -i http://localhost:8888/bar -H 'Authorization: Bearer 1523f55f-381c-4395-b004-93f4991ca208'
```

#### Response
```http
HTTP/1.1 401 Unauthorized
Server: TornadoServer/4.0.2
Date: Wed, 26 Nov 2014 19:26:17 GMT
Content-Type: text/html; charset=UTF-8
Content-Length: 24
Proxy-Connection: keep-alive
Connection: keep-alive

ResourceNotInClientScope
```
