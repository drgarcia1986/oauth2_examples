# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

import base64
from simple_oauth2.server.client.base import InvalidClientCredentials, ClientUnauthorized, ResourceNotInClientScope
from simple_oauth2.utils.types import GrantTypes
import inspect
from simple_oauth2.utils.log import server_controller_log


class InvalidGrantType(Exception):
    pass


class OAuth2Controller():
    def __init__(self, client_controller, token_controller, request_adapter,
                 grant_list=None, validate_scope_in_client_credentials=True):
        self.client_controller = client_controller
        self.token_controller = token_controller
        self.grant_list = grant_list if grant_list else []
        self.validate_scope_in_client_credentials = validate_scope_in_client_credentials
        if inspect.isclass(request_adapter):
            self.request_adapter_class = request_adapter
        else:
            self.request_adapter_class = request_adapter.__class__

    def __get_client_credentials(self, request_adapter):
        if request_adapter.authorization:
            client_id, client_secret = base64.b64decode(request_adapter.authorization).decode().split(':')
        else:
            client_id = request_adapter.arguments.get_by_id('client_id', None)
            client_secret = request_adapter.arguments.get_by_id('client_secret', None)

        if not client_id or not client_secret:
            raise InvalidClientCredentials

        return client_id, client_secret

    def __validate_scopes_in_generate_token(self, grant, requests_scopes, client_scopes):
        if grant == GrantTypes.client_credentials and not self.validate_scope_in_client_credentials:
            return True
        invalid_scope = set(requests_scopes) - set(client_scopes)
        if len(invalid_scope) > 0:
            raise ClientUnauthorized("Client don't have permission for this scopes [{}]".format(', '.join(invalid_scope)))

    def __validate_client_by_grant(self, request_adapter, client_secret, client_info):
        if request_adapter.grant not in client_info['authorized_grants']:
            raise ClientUnauthorized("Client don't have permission for this grant")
        if request_adapter.grant in [GrantTypes.client_credentials]:
            if not client_secret or client_secret != client_info['client_secret']:
                raise InvalidClientCredentials
        self.__validate_scopes_in_generate_token(request_adapter.grant,
                                                 request_adapter.scopes,
                                                 client_info['scopes'])

    def __build_request_adapter(self, request):
        request_adapter = self.request_adapter_class()
        request_adapter.server_grants_list = self.grant_list
        request_adapter.request = request
        request_adapter.read_arguments()
        request_adapter.read_authorization()

        return request_adapter

    def validate_and_generate_token(self, request):
        request_adapter = self.__build_request_adapter(request)
        request_adapter.read_grant()
        request_adapter.read_scopes()
        client_id, client_secret = self.__get_client_credentials(request_adapter)
        client_info = self.client_controller.get_by_id(client_id)
        self.__validate_client_by_grant(request_adapter, client_secret, client_info)
        token = self.token_controller.create(client_id,
                                             request_adapter.grant,
                                             request_adapter.scopes)
        return token

    def __validade_scope_in_request(self, scope, token):
        if token['grant_type'] == GrantTypes.client_credentials and not self.validate_scope_in_client_credentials:
            return True
        elif scope and scope not in token['scopes']:
            raise ResourceNotInClientScope
        else:
            return True

    def validate_token(self, request, scope=None):
        request_adapter = self.__build_request_adapter(request)
        request_adapter.read_token()
        token = self.token_controller.get(request_adapter.token)
        self.__validade_scope_in_request(scope, token)
        return True

    def revoke_token(self, request):
        request_adapter = self.__build_request_adapter(request)
        request_adapter.read_token()
        token_info = self.token_controller.get(request_adapter.token)
        client_id, client_secret = self.__get_client_credentials(request_adapter)
        client_info = self.client_controller.get_by_id(client_id)
        if token_info['client_id'] == client_info['client_id']:
            self.token_controller.revoke_by_token(request_adapter.token)
            return request_adapter.token
