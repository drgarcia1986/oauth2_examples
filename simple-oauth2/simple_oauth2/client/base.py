# !/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'Diego Garcia'

import json
import base64
import urllib.parse
import datetime
from simple_oauth2.utils.types import GrantTypes


class OAuth2HTTPResponse():
    def __init__(self, status, reason, content=None):
        self.status = status
        self.reason = reason
        self.content = content


class OAuth2HTTPResponseError(OAuth2HTTPResponse, Exception):
    pass


class OAuth2HTTPClientBase():
    def __init__(self, server, uri_token, uri_revoke_token, grant_type, client_id, client_secret=None,
                 scope=None, security=True, authorization_header=True,
                 token=None, token_type=None, token_expiration=None):
        self.grant_type = grant_type
        self.client_id = client_id
        self.client_secret = client_secret
        self.server = server
        self.uri_token = uri_token
        self.uri_revoke_token = uri_revoke_token
        self.scope = scope
        self.authorization_header = authorization_header
        self.protocol = 'https' if security else 'http'
        self.token = token
        self.token_type = token_type
        self.token_expiration = token_expiration
        self.__token_strategy = {GrantTypes.client_credentials: self.get_token_client_credentials}

    def make_authorization(self, headers, body):
        if self.authorization_header:
            authorization = base64.b64encode(bytes('{}:{}'.format(self.client_id, self.client_secret), 'utf-8'))
            headers.update({'Authorization': 'Basic {}'.format(authorization.decode())})
        else:
            body.update({"client_id": self.client_id, "client_secret": self.client_secret})

    def token_expired(self):
        if self.token_expiration:
            return self.token_expiration <= datetime.datetime.now()
        else:
            return True

    def process_body_response(self, status, reason, headers, body):
        body_response = None
        if len(body) > 0:
            if 'application/json' in headers['Content-Type'].lower():
                body_response = json.loads(body)
            else:
                try:
                    body_response = {values.split('=')[0]: values.split('=')[1]
                                     for values in body.split('&')}
                except:
                    body_response = body
        return OAuth2HTTPResponse(status, reason, body_response)

    def make_request(self, uri, headers, method, body):
        raise NotImplemented

    def make_request_encode_multipart_formdata(self, uri, headers, method, body):
        raise NotImplemented


    def get_token_client_credentials(self):
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        body = {"grant_type": "client_credentials"}
        if self.scope:
            body.update({'scope': ' '.join(self.scope)})
        self.make_authorization(headers, body)
        body = urllib.parse.urlencode(body)

        response = self.make_request(uri=self.uri_token,
                                     headers=headers,
                                     method='POST',
                                     body=body)

        if response.status != 200:
            raise OAuth2HTTPResponseError(response.status, response.reason, response.content)

        self.token = response.content['access_token']
        self.token_type = response.content['token_type']
        seconds = response.content.get('expires_in', None)
        if seconds:
            self.token_expiration = datetime.datetime.now() + datetime.timedelta(seconds=seconds)

        return True

    def revoke_token(self):
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        body = {"access_token": self.token}
        self.make_authorization(headers, body)
        body = urllib.parse.urlencode(body)

        response = self.make_request(uri=self.uri_revoke_token,
                                     headers=headers,
                                     method='POST',
                                     body=body)

        if response.status == 200:
            self.token, self.token_expiration = None, None
            return True
        else:
            raise OAuth2HTTPResponseError(response.status, response.reason, response.content)

    def get_token(self):
        func = self.__token_strategy[self.grant_type]
        return func()

    def consume_resource(self, uri, method='GET', body=None):
        if not self.token or self.token_expired():
            self.get_token()

        headers = {'Authorization': '{} {}'.format(self.token_type, self.token)}
        if body:
            headers.update({'Content-Type': 'application/json'})

        body = json.dumps(body) if body else None

        response = self.make_request(uri=uri,
                                     method=method,
                                     headers=headers,
                                     body=body)

        return response